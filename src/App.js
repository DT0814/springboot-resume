import React, { Fragment } from "react";
import './app.css'
import Input from "./Input";
import GuessButton from "./GuessButton";
import CreateButton from "./CreateButton";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locationStr: '',
      record: [],
      current: []
    };
    this.changeCurrent = this.changeCurrent.bind(this);
  }

  handlerChangeLocation(loactionStr) {
    this.setState({
      locationStr: loactionStr,
      record:[],
    })
  }
  changeRecord(value){
    this.setState({
      record:[...this.state.record,[...this.state.current,value]]
    })
  }
  changeCurrent(index, num) {
    let temp = this.state.current;
    temp[index] = num;
    this.setState({
      current: temp
    })
  }

  render() {
    console.log(this.state);
    return (
      <Fragment>
        <CreateButton changeLoaction={this.handlerChangeLocation.bind(this)}/>
        <br/>
        <table>
          <tr key='headerTr'>
            <th><Input changeCurrent={this.changeCurrent} index='0'/></th>
            <th><Input changeCurrent={this.changeCurrent} index='1'/></th>
            <th><Input changeCurrent={this.changeCurrent} index='2'/></th>
            <th><Input changeCurrent={this.changeCurrent} index='3'/></th>
            <th></th>
          </tr>
          {
            this.state.record.length !== 0 && this.state.record.map((value, index) => {
              return (
                <tr key={'tr' + index}>
                  <td key={'td0' + index}><span key={'span' + index} className='span-class'>{value[0]}</span></td>
                  <td key={'td1' + index}><span key={'span' + index} className='span-class'>{value[1]}</span></td>
                  <td key={'td2' + index}><span key={'span' + index} className='span-class'>{value[2]}</span></td>
                  <td key={'td3' + index}><span key={'span' + index} className='span-class'>{value[3]}</span></td>
                  <td key={'td4' + index}><span key={'span' + index} className='span-class'>{value[4]}</span></td>
                </tr>
              )
            })
          }
        </table>
        <br/>
        <GuessButton locationStr={this.state.locationStr} changeRecord={this.changeRecord.bind(this)} current={this.state.current} record={this.state.record}/>
      </Fragment>
    );
  }
}

export default App;
