import React from 'react'
import axios from "axios";

export default class GuessButton extends React.Component {

  playGameClick() {
    if (this.props.locationStr.length === 0) {
      alert('please create game');
      return;
    }
    let props=this.props;
    axios.patch(this.props.locationStr, {
      answer: this.props.current.join(""),
    })
      .then(function (response) {
        props.changeRecord(response.data.hint);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <button onClick={this.playGameClick.bind(this)} className='button-class'>Guess</button>
    )
  }
}
