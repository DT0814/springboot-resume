import axios from 'axios'

let http = {
    post: "",
    get: ""
};

http.post = function (api, data) {
    return new Promise((resolve, reject) => {
        axios.post(api, data).then((res) => {
            resolve(res);
        })
    })
};
http.patch = function (api, data) {
    console.log(data);
    return new Promise((resolve, reject) => {
        axios.patch(api, data).then((res) => {
            resolve(res);
        })
    })
};

http.get = function (api, data) {
    return new Promise((resolve, reject) => {
        axios.get(api, data).then((res) => {
            resolve(res);
        })
    })
};
export default http
