import React from 'react'
import http from "./http";
import axios from "axios";

export default class CreateButton extends React.Component {

  handlerClick() {
    let props=this.props;
    axios.post("/api/games/", {
    })
      .then(function (response) {
        props.changeLoaction(response.headers.location)
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <button onClick={this.handlerClick.bind(this)} className='button-class'>New Game</button>
    )
  }
}
