import React from 'react'

export default class Input extends React.Component {
  changeCurrent(event) {
    this.props.changeCurrent(this.props.index,event.target.value);
  }

  render() {
    return (
      <input onChange={this.changeCurrent.bind(this)} value={this.props.currentValue} className='input-class'
             type="text"/>
    )
  }
}
